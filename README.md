# CTA templates for blogs
Please copy the following code and paste it into the wordpress post editor. Make sure that the tab is set to "text" and not "visual" in the wordpress text editor.

`<div class="blog-cta"><hr><div class="blog-cta__content">Here is some dummy text, normally there would be a bit of blurb about a trend report</div><a class="blog-cta__cta">Download for free now!</a><hr></div>`

Please replace the copy as you see fit.